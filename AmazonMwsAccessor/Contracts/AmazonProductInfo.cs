﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonMwsAccessor.Contracts
{
    public class AmazonProductInfo
    {
        public string SellerSku { get; set; }
        public AmazonMarketplaceAsin Asin { get; set; }
        public string Title { get; set; }
        public List<AmazonSalesRanking> SalesRankings { get; set; }
        public int SellerFeedbackCount { get; set; }
        public string FufillmentChannel { get; set; }
        public string ShippingTime { get; set; }
        public decimal SellerPositiveFeedbackRating { get; set; }
        public List<AmazonMarketplaceAsin> AsinValues { get; set; }
        public AmazonDimensions ItemDimensions { get; set; }
        public AmazonDimensions PackageDimensions { get; set; }
        public int CompetitivePriceId { get; set; }
        public string CompetitivePriceCondition { get; set; }
        public AmazonPrice ListPrice { get; set; }
        public AmazonPrice ShippingPrice { get; set; }
        public AmazonPrice LandedPrice { get; set; }
        public Dictionary<string,int> OfferListingCounts { get; set; }
        public int PackageQty { get; set; }
        public string PartNumber { get; set; }
        public string ProductGroup { get; set; }
        public string ProductTypeName { get; set; }
        public List<AmazonOfferInfo> ProductOffers { get; set; }
    }

    public class AmazonOfferInfo
    {
        public bool IsFulfilledByAmazon { get; set; }
        public bool IsBuyBoxWinner { get; set; }

    }

    public class AmazonSalesRanking
    {
        public int SalesRanking { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public class AmazonMarketplaceAsin
    {
        public string MarketplaceId { get; set; }
        public string Asin { get; set; }
    }

    public class AmazonDimensions
    {
        public decimal Weight { get; set; }
        public decimal Length { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
    }

    public class AmazonPrice
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
