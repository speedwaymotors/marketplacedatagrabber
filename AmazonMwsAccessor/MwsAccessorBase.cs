﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarketplaceWebServiceProducts;
using MarketplaceWebServiceProducts.Model;


namespace AmazonMwsAccessor
{
    public class MwsAccessorBase
    {
        protected const string AccessKey = "AKIAIXZIGTIAMRWUS7MQ";
        protected const string SecretKey = "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";
        protected const string MarketplaceId = "ATVPDKIKX0DER";
        protected const string SellerId = "A1FJ1WMPKJVUMQ";
        protected const string ServiceUrl = "https://mws.amazonservices.com";
       

        protected readonly MarketplaceWebServiceProductsClient _client;

        public MwsAccessorBase()
        {
            var config = new MarketplaceWebServiceProductsConfig()
            {
                ServiceURL = ServiceUrl,
                SignatureMethod = "HmacSHA256",
                SignatureVersion = "2",
            };   //Optional config file
            _client = new MarketplaceWebServiceProductsClient(AccessKey, SecretKey, config);
        }



        public string CalculateSignature(string httpMethod, Uri requestUri, List<string> bodyParameters,string secret)
        {

            StringBuilder canonicalQueryBuilder = new StringBuilder();

            canonicalQueryBuilder.AppendLine(httpMethod);
            canonicalQueryBuilder.AppendLine(requestUri.Host);

            canonicalQueryBuilder.AppendLine(requestUri.LocalPath);

            var queryComponents = requestUri.Query.Split('&');

            if (queryComponents.Length > 0 && queryComponents[0].Length > 1 && queryComponents[0][0] == '?')
                queryComponents[0] = queryComponents[0].Remove(0, 1);

            var sortedQuery = queryComponents.ToList();
            sortedQuery.AddRange(bodyParameters);
            sortedQuery.Sort(StringComparer.Ordinal);

            var sortedQueryString = string.Join("&", sortedQuery);

            canonicalQueryBuilder.AppendLine(sortedQueryString);
            var canonicalQuery = canonicalQueryBuilder.ToString();


            return canonicalQuery.HmacSha256Digest(secret);
        }


      

    }
}
