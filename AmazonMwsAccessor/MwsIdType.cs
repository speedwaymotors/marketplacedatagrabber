﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonMwsAccessor
{
    public enum MwsIdType
    {
        ASIN, 
        GCID, 
        SellerSKU, 
        UPC, 
        EAN, 
        ISBN, 
        JAN
    }
}
