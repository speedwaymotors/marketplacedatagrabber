﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarketplaceWebServiceProducts;
using MarketplaceWebServiceProducts.Model;
using System.Net;
using System.Collections.Specialized;

namespace AmazonMwsAccessor
{
    public class MwsProductAccessor : MwsAccessorBase 
    {

        private const string SERVICE_URL = "https://mws.amazonservices.com";
        private readonly MarketplaceWebServiceProductsClient _productClient;

        private const string SELLER_ID = "A1FJ1WMPKJVUMQ";
        private const string MARKETPLACE_ID = "ATVPDKIKX0DER";

        private const string ACCESS_KEY = "AKIAIXZIGTIAMRWUS7MQ";
        private const string SECRET_KEY = "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";

        public MwsProductAccessor() : base()
        {
            //_serviceUrl = $"{ServiceUrl}/Products/2011-10-01";

            var config = new MarketplaceWebServiceProductsConfig()
            {
                SignatureMethod = "HmacSHA256",
                ServiceURL = SERVICE_URL
            };

            _productClient = new MarketplaceWebServiceProductsClient("MWSTestPuller", "1.0", ACCESS_KEY, SECRET_KEY, config);
        }

        //ListMatachingProducts
        public IEnumerable<string> ListMatchingAmazonProductsForQuery(string query)
        {

            var request = new ListMatchingProductsRequest(SELLER_ID, MARKETPLACE_ID, query);
            var response = _productClient.ListMatchingProducts(request);
            var productList = response.ListMatchingProductsResult.Products.Product;

            //TODO: Process Products
            // productList.Select(processProducts)
            return new string[] { "Success!" };

        }

        //GetCompetitivePricingForASIN
        // Amazon throttle - 36000 requests per hour, quota 20 requests, restore 10 items every second
        public string GetCompetitivePricingForAsin(List<string> asin)
        {
            var asinListType = new ASINListType
            {
                ASIN = asin
            };
            var request = new GetCompetitivePricingForASINRequest(SELLER_ID, MarketplaceId, asinListType);

            var response = _productClient.GetCompetitivePricingForASIN(request);

            var result = response.GetCompetitivePricingForASINResult.First();

            return "Success";
        }
        //GetLowerstOfferListingsForASIN
        // Amazon throttle - 36000 requests per hour, quota 20 requests, restore 10 items every second
        public string GetLowestOfferListingsForAsin(List<string> asin)
        {
            var asinListType = new ASINListType
            {
                ASIN = asin
            };
            var request = new GetLowestOfferListingsForASINRequest(SELLER_ID, MarketplaceId, asinListType);

            var response = _productClient.GetLowestOfferListingsForASIN(request);

            var result = response.GetLowestOfferListingsForASINResult.First();

            return "Success";
        }

        //GetLowestPricedOffersForAsin
        // Amazon throttle - 200 requests per hour, quota 10 requests, restore 5 items every second
        public string GetLowestPricedOffersForAsin(string asin, string itemCondition)
        {

            var request = new GetLowestPricedOffersForASINRequest(SELLER_ID, MarketplaceId, asin, itemCondition);

            var response = _productClient.GetLowestPricedOffersForASIN(request);

            var result = response.GetLowestPricedOffersForASINResult;

            return "Success";
        }



        //OLD CODE MIGHT DELETE......

        //public string PullAmazonDataForSku(string sku)
        //{
        //    var idList = new IdListType()
        //    {
        //        Id = { sku }
        //    };

        //    var request = new GetMatchingProductForIdRequest(SellerId, MarketplaceId, MwsIdType.SellerSKU.ToString(), idList);

        //    try
        //    {
        //        var response = _client.GetMatchingProductForId(request);
        //    }
        //    catch (Exception ex)
        //    {
        //        var test = ex.Message;
        //        throw;
        //    }


        //   // response.GetMatchingProductForIdResult.Select(x => x.)


        //    return "";
        //}


        //public string PullAmazonDataForMpn(string mpn, string brand)
        //{
        //    var idList = new IdListType()
        //    {
        //        Id = { mpn }
        //    };

        //    var request = new GetMatchingProductForIdRequest(SellerId, MarketplaceId, MwsIdType.SellerSKU.ToString(), idList);

        //    var response = _client.GetMatchingProductForId(request);



        //    return "";
        //}

        ////public async Task<string> PullAmazonDataForMpnWebClient(string mpn, string brand)
        ////{

        ////    var webClient = new WebClient();
        ////    var requestUri = new Uri(_serviceUrl);

        ////    var parametersList = new List<string>();

        ////    parametersList.Add("AWSAccessKeyId=AKIAIXZIGTIAMRWUS7MQ");
        ////    parametersList.Add("Action=GetMatchingProductForId");
        ////    parametersList.Add("IdList.Id.1=47552024");
        ////    parametersList.Add("IdType=SellerSKU");
        ////    parametersList.Add("MarketplaceId=ATVPDKIKX0DER");
        ////    parametersList.Add("SellerId=A1FJ1WMPKJVUMQ");
        ////    parametersList.Add("SignatureMethod=HmacSHA256");
        ////    parametersList.Add("SignatureVersion=2");
        ////    parametersList.Add("Timestamp=2018-08-25T19:00:12Z");
        ////    parametersList.Add("AWSAccessKeyId=AKIAIXZIGTIAMRWUS7MQ");

        ////    webClient.Headers.Add(HttpRequestHeader.ContentType, "x-www-form-urlencoded");
        ////    //webClient.Headers.Add(HttpRequestHeader.UserAgent, "");

        ////    var xmlString = await webClient.UploadStringTaskAsync(_serviceUrl,"");
        ////}




    }
}
