﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using AmazonMwsAccessor.Contracts;

namespace AmazonMwsBulkProcessor
{
    class Program
    {
        static void Main(string[] args)
        {

            var queries = GetQueryStrings();

            Console.WriteLine("Your Queries are: ");
            foreach(var query in queries)
            {
                Console.WriteLine(query);
            }

            Console.WriteLine("\n\n\n");

            //var accessor = new AmazonMwsAccessor.MwsProductAccessor();
            ////var results = accessor.PullAmazonDataForSku("917347-22");
            //var result = accessor.ListMatchingAmazonProductsForQuery("tires");
            //Console.WriteLine(result);

            var productList = new List<AmazonProductInfo>()
            {
                new AmazonProductInfo()
                {
                    SellerSku = "91004-RED",
                    //Asin = "BOXDISK",
                    Title = "Pop Rivets"
                },
                new AmazonProductInfo()
                {
                    SellerSku = "123456",
                   // Asin = "BXKDEOD",
                    Title = "Part # 2"
                },
                new AmazonProductInfo()
                {
                    SellerSku = "4558874",
                   //Asin = "BR3DKS",
                    Title = "Part # 3"
                },
            };

            var filePath = "Output.csv";
            Console.WriteLine($"Outputing Results to file: {filePath}");
            CreateCsvOutputFile(filePath, productList);
            Console.WriteLine("Done writing file!");

            Console.WriteLine("Done");
            Console.ReadLine();
        }


        public static List<string> GetQueryStrings()
        {
            Console.WriteLine("Enter query stings or 'GO' to start processing");

            var queryList = new List<string>();
            var input = Console.ReadLine();

            while(input.ToLower() != "go")
            {
                queryList.Add(input);
                input = Console.ReadLine();
            }

            return queryList;
        }


        public static void CreateCsvOutputFile<T>(string filePath,List<T> outputRows)
        {
            using (var streamWriter = new StreamWriter(filePath))
            using (var writer = new CsvWriter(streamWriter))
            {
                writer.WriteRecords(outputRows);
                //foreach(var row in outputRows)
                //{
                //    writer.writer
                //    writer.WriteRecord(row);
                //}
            }

        }
    }
}
